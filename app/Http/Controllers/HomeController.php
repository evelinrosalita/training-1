<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Article;

class HomeController extends Controller
{

	public function getIndex()
	{
			$article = Article::all();
			return view('welcome', compact('article'));
	}
}
